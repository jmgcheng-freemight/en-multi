<?php 
	get_header();
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/common/css/order.css" type="text/css">
<script src="<?php echo get_template_directory_uri();?>/js/jquery-modal-master/jquery.modal.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/jquery-modal-master/jquery.modal.css" type="text/css" media="screen" />
<script src="<?php echo get_template_directory_uri();?>/js/order.js" type="text/javascript" ></script>
	        <!--+++.container+++-->
	        <div class="container cf">

	            <!--++.main/++-->
	            <div class="upsell-container">
	            	<?php 
	            		while ( have_posts() ) : the_post();
							$id = get_the_ID();
							$upsell_number = get_post_meta( $id, 'upsell_number',true );
							$upsell_product_name = get_post_meta( $id, 'upsell_product_name',true );
							$original_price = get_post_meta( $id, 'original_price', true );
							$promo_price = get_post_meta( $id, 'promo_price', true );
							$additional_message = get_post_meta( $id, 'additional_message', true );
							$agree_message = get_post_meta( $id, 'agree_message', true );
						echo "<input type='hidden' class='upsell-number' value='".$upsell_number."'/>";
						echo "<input type='hidden' class='upsell-name' value='".$upsell_product_name."'/>";
						echo "<div class='upsell-content'>";
							the_content();
						echo "</div>";
						echo "<div class='upsell-sub-content'>";
							echo "<div class='upsell-images'>";
								if( has_post_thumbnail() ) :
									the_post_thumbnail();
									$second_image = MultiPostThumbnails::get_post_thumbnail_url('upsell', 'secondary-image');
									echo "<img src='".$second_image."' />" ;
								endif ;
							echo "</div>";
							echo "<div class='upsell-details'>";
								echo $additional_message;
								echo "<br/>";
								echo "<p class='original-price-container'>通常価格：".$original_price."円（税込）</p>";
								if($promo_price){
									echo "<p class='promo-price-container'>限定価格：".$promo_price."円（税込）</p>";
									echo "<input type='hidden' class='upsell-price' name='price' value='".$promo_price."'/>";
									$upsell_price = $promo_price;
								}	else {
									echo "<input type='hidden' class='upsell-price' name='price' value='".$original_price."'/>";
									$upsell_price = $original_price;
								}							
								echo "<div class='agreement-container'>";
									echo "<div><input type='checkbox' name='agree_upsell' id='agree_upsell'/>  ".$agree_message ."</div>"; 
								echo "</div>";
							echo "</div>";
						echo "</div>";
						echo "<div class='submit-container'><a href='#order_details' class='upsell-submit' rel='modal:open'><img src='".get_stylesheet_directory_uri()."/images/upsell-submit.png'/></a><div>";
						endwhile;
	            	?>
					<?php 
						global $wpdb;
						$sendid = $_GET['key'];
						$myrows = $wpdb->get_results( "SELECT * FROM order_data WHERE sendid =".$sendid." ORDER BY id DESC LIMIT 1" );
						$orders = json_decode(json_encode($myrows), True);
						$order = $orders[0];
					?>
					<!-- Modal HTML embedded directly into document -->
					<div id="order_details" style="display:none;">
						<div class="header" style="width:100%;">
							<h1>お問い合わせフォーム > 確認画面</h1>
							<h2>以下の内容でよろしければ「送信する」ボタンを押して下さい。</h2>
						</div>
						<?php 
							$address = split(" ",$order['address']); 
							$name = split(" ",$order['order_name']); 
							$name_e = split(" ",$order['order_phonetic_name']); 
							$last_name = substr(strstr($order['order_name']," "), 1);
							$mei = substr(strstr($order['order_phonetic_name']," "), 1);
							$building_name = substr(strstr($order['address']," "), 1);
						?>
						<table>
						
							<tr><td>お支払い方法</td><td><?php echo $order['payment_method']; ?></td></tr>
							<tr class="upsell-tr" style="display:none;"><td>追加商品</td><td><?php echo $upsell_product_name.' '.$upsell_price.'円' ?></td></tr>
							<tr><td>氏名</td><td ><?php echo $order['order_name']; ?></td></tr>
							<tr><td>氏名（フリガナ）</td><td ><?php echo $order['order_phonetic_name']; ?></td></tr>
							<tr><td>郵便番号</td><td ><?php echo $order['postal_code']; ?></td></tr>
							<tr><td>住所</td><td ><?php echo $address[0]; ?></td></tr>
							<tr><td>建物名</td><td ><?php echo substr(strstr($order['address']," "), 1); ?></td></tr>
							<tr><td>電話番号</td><td ><?php echo $order['tell_no']; ?></td></tr>
							<tr><td>メールアドレス</td><td ><?php echo $order['email']; ?></td></tr>							
						</table>							
							<input type="hidden" class="order-sendid" value="<?php echo $order['sendid']; ?>">
							<input type="hidden" class="order-tell-no" value="<?php echo $order['tell_no']; ?>">
							<input type="hidden" class="order-e-mail" value="<?php echo $order['email']; ?>">
							<input type="hidden" class="order-amount" value="<?php echo $order['amount']; ?>">
							<input type="hidden" class="order-original-amount" value="<?php echo $order['amount']; ?>">
							<input type="hidden" class="order-payment" value="<?php echo $order['payment_method']; ?>">
							<input type="hidden" class="order-shipping" value="<?php echo $order['shipping_cost']; ?>">
							<input type="hidden" class="order-delivery" value="<?php echo $order['delivery_method']; ?>">
							<input type="hidden" class="order-pid" value="<?php echo $order['pid']; ?>">
							<input type="hidden" class="order-pcode" value="<?php echo $order['product_code']; ?>">
							<input type="hidden" class="order-pname" value="<?php echo $order['product_name']; ?>">
							<input type="hidden" class="order-surname" value="<?php echo $name[0]; ?>">
							<input type="hidden" class="order-name" value="<?php echo $last_name; ?>">
							<input type="hidden" class="order-say" value="<?php echo $name_e[0]; ?>">
							<input type="hidden" class="order-mei" value="<?php echo $mei; ?>">
							<input type="hidden" class="order-postal-code" value="<?php echo $order['postal_code']; ?>">
							<input type="hidden" class="order-prefecture" value="">
							<input type="hidden" class="order-address" value="<?php echo $address[0]; ?>">
							<input type="hidden" class="order-building-name" value="<?php echo $building_name; ?>">
						<a class="btn-back" href="javascript:void(0)" rel="modal:close">< 前に戻る</a>
						<a class="btn-final-submit" href="javascript:void(0)" >送信する ></a>
					</div>
					<!-- hidden form for Credit Card payment-->
					<FORM METHOD="POST" ACTION="https://linkpt.cardservice.co.jp/cgi-bin/credit/order.cgi" id="formCreditCard" TARGET="_top" style="display:none;"> 
						<INPUT TYPE="hidden" NAME="clientip" VALUE="2019000801">
						<INPUT TYPE="hidden" NAME="money" class="order-credit-amount" value="<?php echo $order['total_amount']; ?>">
						<INPUT TYPE="hidden" NAME="sendid" class="credit_sendid" VALUE="<?php echo $order['sendid']; ?>">
						<INPUT TYPE="hidden" NAME="telno" class="credit_tell_no" VALUE="<?php echo $order['tell_no']; ?>">
						<INPUT TYPE="hidden" NAME="email" class="credit_email" VALUE="<?php echo $order['email']; ?>">
						<INPUT TYPE="hidden" NAME="success_url" VALUE="<?php echo get_site_url();?>/thank-you-message/">
						<INPUT TYPE="hidden" NAME="success_str" VALUE="トップページへ">
						<INPUT TYPE="hidden" NAME="failure_url" VALUE="<?php echo get_site_url();?>"> 
						<INPUT TYPE="hidden" NAME="failure_str" VALUE="トップページへ">
						<INPUT TYPE="submit" VALUE="クレジット決済ページ">
					</FORM>
	            </div>
	            <!--/.main-->
	            <!--++/.main++-->


	        </div>
	        <!--+++/.container+++-->


<?php 
	get_footer(); 
?>	