 <?php
 
 //check if page is using order form template
function is_order_form($id){
	$template = get_page_template_slug( $post->ID );
	$template_array = explode("/", $template);
	if($template_array[1] == 'order-form.php')return true; else return false;
}

function is_upsell($id){
	if(get_post_type( $id ) == 'upsell')return true; else return false;
}

//specific style for order form only
function order_style(){
	$style = '<style>
.hissu {
	background: red;
	border: 1px solid red;
	border-radius: 8px;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	color: #FFFFFF;
	width: 35px;
	padding: 2px 0;
	text-align:center;
	font-size:10px;
	float:right;
}

.backimg{
	background-image:url("'.get_template_directory_uri().'/image/list/handlift.png");
	background-repeat:no-repeat;
	background-position:right bottom -5px;
	background-size:160px;
}

.backalp{
	background: rgba(255, 255, 255, 0.7);
}

input{
	background-color:#CCCCCC;
}

input:focus {
	background-color:#FFFFFF;
}

#impt_back{
	background-image: url("'.get_template_directory_uri().'/dream/img/contact_btn_background.png");
	background-repeat: no-repeat;
	background-position: center;
}
#impt{
	background-color: transparent;
}
.fixedWidget {
	position:relative;
	position: fixed;
	top: 0px;
}
</style>';
	 return $style;
 }
 
apply_filters ( 'wp_mail_from', 'staff@freemight.com' );
apply_filters ( 'wp_mail_from_name', 'Hand-lift'); //if doesn't work edit wp-includes/pluggable.php line 312

function send_order() {
	
	$order = $_POST['order'];
	send_handlift_mail($order['sendid'],$order['email']);
	
	//update available stock and sold units
	$av_stock = get_post_meta( $pid, 'av_stock', '' );
	$av_stock = $av_stock[0] - 1;
	update_post_meta($pid,'av_stock',$av_stock); 

	$sold_unit = get_post_meta( $pid, 'sold_units', '' );
	if($sold_unit):
		$sold_unit = $sold_unit[0] + 1; 
		update_post_meta($pid,'sold_units',$sold_unit);
	else:
		$sold_unit = 1; 
		update_post_meta($pid,'sold_units',$sold_unit);
	endif;

	echo json_encode(array('order_no' => $order_no));
	wp_die();
}

// ajax send order to email
add_action( 'wp_ajax_nopriv_send_order', 'send_order' );
add_action( 'wp_ajax_send_order', 'send_order' );

function update_order() {
	global $wpdb;
	$data = $_POST['data'];
	
	$sendid = $data['sendid'];
	$upsell_name = $data['upsell_name'];
	$upsell_number = $data['upsell_number'];
	$sendid = $data['sendid'];
	$amount = floatval($data['amount']);
	$shipping_cost = floatval($data['shipping_cost']);
	$upsell_amount = floatval($data['upsell_amount']);
	 
	$new_amount = floatval($amount + $shipping_cost + $upsell_amount);
	
	$wpdb->update('order_data', array('upsell_name'=>$upsell_name,'upsell_number'=>$upsell_number,'upsell_amount'=>$upsell_amount,'total_amount'=>$new_amount), array('sendid'=>$sendid));

	echo json_encode($data);
	wp_die();	
}

// ajax send order to email
add_action( 'wp_ajax_nopriv_update_order', 'update_order' );
add_action( 'wp_ajax_update_order', 'update_order' );

function save_order() {
	global $wpdb;
	
	date_default_timezone_set('Asia/Tokyo');
	
	$order = $_POST['order'];
	$pid = $order['product_id'];
	$now = date('Y/m/d H:i:s');
	$y = date('Y');
	$m = date('m');
	$d = date('d');
	$h = date('G');
	$i = date('i');
	$s = date('s');

	//generate order number
	$myrows = $wpdb->get_results( "SELECT order_id FROM order_ctr ORDER BY order_id DESC LIMIT 1" );
	$sendid = 0;
	if($myrows == null){
		$order_no = 'HAND0000001';
		$insert_data = array( 
				'order_number' => $order_no, 
				'order_log' => $now 
				);
		$wpdb->insert('order_ctr', $insert_data,'' );
		$sendid = 1;
	}else{
		$order_no = generate_order_no($myrows[0]);
		$insert_data = array( 
				'order_number' => $order_no, 
				'order_log' => $now 
				);
		$wpdb->insert('order_ctr', $insert_data,'' );
		$sendid = $myrows[0]->order_id;
	}		
	//construct email and order post content
	$total = $order['item_amount'] + $order['shipping_charge'];

	
	$sendid = intval($sendid) + 2736;
	
	$order_data = array( 
			'pid' => $pid, 
			'sendid' => $sendid, 
			'order_no' => $order_no, 
			'order_date' => $y.'年'.$m.'月'.$d.'日 '.$h.'時'.$i.'分'.$s.'秒',
			'order_name' => $order['surname'].' '.$order['name'],
			'order_phonetic_name' => $order['say'].' '.$order['mei'],
			'postal_code' => $order['postal_code'],
			'address' => $order['address'].' '.$order['building_name'],
			'tell_no' => $order['telephone_number'],
			'email' => $order['email'],
			'payment_method' => $order['payment_method'],
			'product_code' => $order['product_code'],
			'product_name' => $order['product_name'],
			'amount' => $order['item_amount'],
			'shipping_cost' => $order['shipping_charge'],
			'total_amount' => $total,
			'delivery_method' => $order['delivery_method']
			);
	$wpdb->insert('order_data', $order_data,'' );
	
	echo json_encode(
		array(
			'status' => 'Success',
			'order_data' => $order_data,
			'sendid' => $sendid,
		)
	);
	
	wp_die();
}
// ajax send order to email
add_action( 'wp_ajax_nopriv_save_order', 'save_order' );
add_action( 'wp_ajax_save_order', 'save_order' );

function generate_order_no($row){
	
	$id = $row->order_id;
	$i = $id + 1;
	$pre = get_custom('order_prefix');
	return $pre.sprintf("%07d", $i);
}