<?php 
	get_header();
	
?>


			        <!--+++.container+++-->
			        <div class="container cf">


						<?php 
							get_sidebar(); 
						?>


			            <!--++.main/++-->
			            <div class="main">

			                <?php if ( have_posts() ) : ?>
			                	<?php 
			                		while( have_posts() ) :
			                			the_post();
			                	?>

					                		<?php
												the_content();
					                		?>

			                	<?php 
			                		endwhile ;
			                	?>
			            	<?php endif ; ?>

			            </div>
			            <!--/.main-->
			            <!--++/.main++-->


			        </div>
			        <!--+++/.container+++-->


<?php 

	get_footer(); 
?>