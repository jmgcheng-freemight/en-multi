<?php
/**
 * Template Name: Order Form
 *
 * @package Hand-lift 1.0
 * @since Hand-lift 1.0
 */
 ?>
 
<?php 
	get_header();
	
?>
<!-- 必須項目カウント表示個所がついてくる処理 -->
<script type="text/javascript">
	window.onload = function(){
		var $lastWidget = document.getElementById( 'required_box' );
		var $distanceFromTheTop = $lastWidget.getBoundingClientRect().top + window.pageYOffset;
	window.onscroll = function(){
		if( window.pageYOffset > $distanceFromTheTop ) {
			//$("#required_box").css("position","");
			$lastWidget.setAttribute( 'class', 'fixedWidget' );
			//$("#required_box").css("position","fixed");
			//$("#required_box").css("top","0px");
			//$("#required_box").css("left","50%");
		} else {
			$lastWidget.setAttribute( 'class', '' );
		}
            }
	}
</script>
<!-- 必須項目カウント表示個所がついてくる処理 end -->

<!-- 必須項目バリデーション処理 -->
<script type="text/JavaScript">
$(function(){
    function conf(){
        //alert("aaa");
		//return false;
		$('form#contact-form').submit();
	
    }

    kntxtext.target.push([ '姓', 'セイ', kntxtext.constant.letterType.kana, kntxtext.constant.insertType.auto ]);
    kntxtext.target.push([ '名', 'メイ', kntxtext.constant.letterType.kana, kntxtext.constant.insertType.auto ]);

    function zen2han(obj){
        // 文字列かチェック
        if(typeof(obj.value)!="string") return false;
        var word = obj.value;
        obj.value = word.replace(/[！＂＃＄％＆＇（）＊＋，－．／０-９：；＜＝＞？＠Ａ-Ｚ［＼］＾＿｀ａ-ｚ｛｜｝～]/g, function(s) {
            return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);    
        })
    }

    function countMaster() {
        var cnt = 0;
        var myObj = new Object();
        myObj.validCount = 13;
        myObj.validList = {
　　　　    "name1":false, 
　　　　    "name2":false, 
　　　　    "kana1":false, 
　　　　    "kana2":false, 
　　　　    "zip1"  :false, 
　　　　    "zip2"  :false, 
　　　　    "pref" :false, 
　　　　    "address":false, 
　　　　    "tel1"  :false, 
　　　　    "tel2"  :false, 
　　　　    "tel3"  :false, 
　　　　    "email":false,
　　　　    "agree":false
        };

        // 必須項目数をチェックする
        myObj.update = function() {
            var strSei = $("#sei").val();
            if (strSei == "") {
                cntObj.validList.name1 = false;
                $('#sei').css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.name1 = true;
                $('#sei').css({
                    'background-color' : '#fff'
                });
            }

            var strMei = $("#mei").val();
            if (strMei == "") {
                cntObj.validList.name2 = false;
                $("#mei").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.name2 = true;
                $("#mei").css({
                    'background-color' : '#fff'
                });
            }

            var seiKana = $("#sei-kana").val();
            if (seiKana == "") {
                cntObj.validList.kana1 = false;
                $("#sei-kana").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.kana1 = true;
                $("#sei-kana").css({
                    'background-color' : '#fff'
                });
            }

            var meiKana = $("#mei-kana").val();
            if (meiKana == "") {
                cntObj.validList.kana2 = false;
                $("#mei-kana").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.kana2 = true;
                $("#mei-kana").css({
                    'background-color' : '#fff'
                });
            }

            var zipCode1 = $("#zipcode1").val();
            if (zipCode1 == "") {
                cntObj.validList.zip1 = false;
                $("#zipcode1").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.zip1 = true;
                $("#zipcode1").css({
                    'background-color' : '#fff'
                });
            }

            var zipCode2 = $("#zipcode2").val();
            if (zipCode2 == "") {
                cntObj.validList.zip2 = false;
                $("#zipcode2").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.zip2 = true;
                $("#zipcode2").css({
                    'background-color' : '#fff'
                });
            }


            var strPref = $("#prefecture").val();
            if (strPref == '') {
                this.validList.pref = false;
            } else {
                this.validList.pref = true;
            }

            var strAddress = $("#address").val();
            if (strAddress == "") {
                cntObj.validList.address = false;
                $("#address").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.address = true;
                $("#address").css({
                    'background-color' : '#fff'
                });
            }

            var strAddress2 = $("#address2").val();
            if (strAddress2 == "") {
                $("#address2").css({
                    'background-color' : '#ddd'
                });
            } else {
                $("#address2").css({
                    'background-color' : '#fff'
                });
            }

            var strTel1 = $("#telephone1").val();
            if (strTel1 == "") {
                cntObj.validList.tel1 = false;
                $("#telephone1").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.tel1 = true;
                $("#telephone1").css({
                    'background-color' : '#fff'
                });
            }

            var strTel2 = $("#telephone2").val();
            if (strTel2 == "") {
                cntObj.validList.tel2 = false;
                $("#telephone2").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.tel2 = true;
                $("#telephone2").css({
                    'background-color' : '#fff'
                });
            }

            var strTel3 = $("#telephone3").val();
            if (strTel3 == "") {
                cntObj.validList.tel3 = false;
                $("#telephone3").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.tel3 = true;
                $("#telephone3").css({
                    'background-color' : '#fff'
                });
            }

            var strMail = $("#email").val();
            if (strMail == "") {
                cntObj.validList.email = false;
                $("#email").css({
                    'background-color' : '#ddd'
                });
            } else {
                cntObj.validList.email = true;
                $("#email").css({
                    'background-color' : '#fff'
                });
            }
			
            if ($("#agree").prop('checked')) {
				cntObj.validList.agree = true;
                $("#agree").css({
                    'background-color' : '#fff'
                });
                
            } else {
                cntObj.validList.agree = false;
                $("#agree").css({
                    'background-color' : '#ddd'
                });
            }

            var count = 0;
            for (k in myObj.validList) {
                /* console.log(k + ' -value = ' + myObj.validList[k] ); */
                if (myObj.validList[k] == false) {
                    count++;
                }
            }
            if (count > 0) {
                message = "残り" + count + "項目";
                // ボタンを未入力ありに変更(赤)
                $("#confirm_button_disable").show();
                $("#confirm_button").hide();
            } else {
                message = "全て入力されています。";
                // ボタンを入力完了に変更(緑)
                $("#confirm_button_disable").hide();
                $("#confirm_button").show();
            }
            $('#required_box').show();
            var koumoku = "13項目";
            $('#required_box').children("#required_total").text(koumoku);
            $('#required_box').children("#required_remaining").text(message);
        }
        return myObj;
    }		
    cntObj = countMaster();

    jQuery("#contact-form").validationEngine();	
    $("#sei").change(function () {
        jQuery("#sei").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#mei").change(function () {
        jQuery("#mei").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#sei-kana").change(function () {
        jQuery("#sei-kana").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#mei-kana").change(function () {
        jQuery("#mei-kana").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#zipcode1").change(function () {
        jQuery("#zipcode1").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();
    $("#zipcode2").change(function () {
        jQuery("#zipcode2").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#prefecture").change(function(){
        var selecVal = $(this).val();
        if( selecVal == '') {
             $('#prefecture').attr("class","validate[requiredPref]");
             jQuery("#prefecture").validationEngine('attach', {
                promptPosition:"topLeft"
             });
        } 
        cntObj.update();
     })
     .change();

    $("#address").change(function () {
        jQuery("#address").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#address2").change(function () {
        cntObj.update();
     })
     .change();

    $("#telephone").change(function () {
        jQuery("#telephone").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

    $("#email").change(function () {
        jQuery("#email").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();
	 
	 $("#agree").change(function () {
        jQuery("#agree").validationEngine('attach', {
            promptPosition:"topLeft"
        });
        cntObj.update();
     })
     .change();

});
</script>
<!-- 必須項目バリデーション処理 end -->

<script type="text/JavaScript">
$(function(){
 
	//音を鳴らす
	$('.appButton').mouseover(function(){
 
		document.getElementById("overSound").currentTime = 0;
		document.getElementById("overSound").play();
 
	});
 
 
});
</script>
<!-- ボタンの音　ここまで -->

<script type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

	        <!--+++.container+++-->
	        <div class="container cf">


				<?php 
					get_sidebar(); 
				?>


	            <!--++.main/++-->
	            <div class="main">

					<h3 class="ti">商品ご注文</h3>					
					<?php 
						$pid = $_GET["pid"]; //product post id
						
						$load_capacity = get_post_meta( $pid, 'load_capacity', '' );
						$max_height = get_post_meta( $pid, 'max_height', '' );
						$min_height = get_post_meta( $pid, 'min_height', '' );
						$weight = get_post_meta( $pid, 'weight', '' );
						$shipping_charge_val = get_post_meta( $pid, 'shipping', '' );
						$normal_price_val = get_post_meta( $pid, 'original_price', '' );
						$limited_price_val = get_post_meta( $pid, 'limited_price', '' );
						$pcode = get_post_meta( $pid, 'product_number', '' );
						$pname = get_post_meta( $pid, 'product_name', '' );
						$comment = get_post_meta( $pid, 'comment', '' );
						
						$load_capacity = $load_capacity[0].'kg'; 
						$max_height = $max_height[0].'mm';
						$min_height = $min_height[0].'mm'; 
						$weight = $weight[0].'kg';
						$shipping_charge_val = $shipping_charge_val[0];
						$shipping_charge = number_format($shipping_charge_val).'円';
						
						$normal_price_val = $normal_price_val[0];
						$limited_price_val = $limited_price_val[0];
						$normal_price = number_format($normal_price_val).'円';
						$limited_price = number_format($limited_price_val).'円';
						
						//not sure about the values below, if same as limited price or differs base on payment method
						//if same as limited price then use limited price no need for variables below
						/* $lumpsum_amount = '39,850円';
						$bank_trans_amount = '39,850円';
						$credit_amount = '39,850円'; */
						
						$delivery_method = 'セイノースーパーエクスプレス';
						
						//getting the upsell link
						$upsell_item_number = get_post_meta( $pid, 'upsell_item_number',true );
						
						$a_args = array(
							'post_type'  		=> 'upsell',
							'meta_key'   		=> 'upsell_number',
							'posts_per_page' 	=> 1,
							'meta_query' => array(
								array(
									'key'     => 'upsell_number',
									'value'   => $upsell_item_number
								),
							),
						);
						$o_query = new WP_Query( $a_args );
						if ( $o_query->have_posts() ) 
						{
								$o_query->the_post();
								$i_post_id = get_the_ID();
								$s_post_permalink = get_permalink($i_post_id);
								$upsell = true;
						} else {
							$upsell = false;
						}
						wp_reset_postdata();
					?>
					<?php 
						//show on dev mode only
						if($_SERVER['HTTP_HOST'] == 'hl.dev'): 
							echo 'pid '.$pid; 
					?>
							<a href="#order_details" class="submit_order" rel="modal:open">test</a>
					<?php endif;?>
					<?php if($upsell):?>
						<input type="hidden" class="upsell_link" value="<?php echo $s_post_permalink;?>">
					<?php else:?>
						<input type="hidden" class="upsell_link" value="">
					<?php endif;?>
					<!-- pass values not included in form input up -->
					<input type="hidden" name="sendid" class="order-sendid" value="">
					<input type="hidden" name="product-id" class="pid" value="<?php echo $pid;?>">
					<input type="hidden" name="product-code" class="pcode" value="<?php echo $pcode[0];?>">
					<input type="hidden" name="product-name" class="pname" value="<?php echo $pname[0];?>">
					<input type="hidden" name="item-amount" class="amount" value="<?php echo $limited_price_val;?>">
					<input type="hidden" name="shipping-charge" class="shipping" value="<?php echo $shipping_charge_val;?>">
					<input type="hidden" name="delivery-method" class="delivery" value="<?php echo $delivery_method;?>">
					
					<!--++.sec/++-->
					<div class="sec2 list_bloc cf">
					 <h4><?php echo $pname[0];?></h4>
					 
					 <div class="list_photo">
					 <?php if (has_post_thumbnail( $pid ) ): ?>
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $pid ), 'medium' ); ?>
						<a href=""><img src="<?php echo $image[0]; ?>" alt="<?php echo $pname[0];?>" /></a>
					<?php else:?>
						<a href=""><img src="<?php echo get_template_directory_uri();?>/image/list/list01.jpg" alt="<?php echo $pname[0];?>" /></a>
					<?php endif; ?>
					
					 </div><!--/.list_photo-->
					 
					 <div class="list_article">
					 <p><strong class="red">1年間の保証付き</strong></p>
					 <h5><?php echo $pname[0];?></h5>
					 <dl>
					  <dt>■店長のコメント■</dt>
					  <dd><?php echo $comment[0];?></dd>
					 </dl>
					 <dl>
					  <dt>■商品仕様■</dt>
					  <dd>・耐荷重：<?php echo $load_capacity;?></dd>
					  <dd>・最高位：<?php echo $max_height;?></dd>
					  <dd>・最低位：<?php echo $min_height;?></dd>
					  <dd>・重　量：<?php echo $weight;?></dd>
					  <dd>・送　料：<?php echo $shipping_charge;?></dd>
					 </dl>
					 <dl class="price">
					  <dd class="normal">通常価格：<?php echo $normal_price;?>（税込）</dd>
					  <dd class="sale"><strong>限定価格：<?php echo $limited_price;?>（税込）</strong></dd>
					  <dd>※現在の在庫が無くなり次第、終了となります。予め、ご了承ください。<br />
					※こちらの商品は、少しでもお安くご提供するため、簡易的な梱包で輸入しており、
					輸送の過程で本体に傷や塗装のはがれ生じてしまいます。傷や塗装のはがれでの商品のご返品は出来かねますため、予めご了承の上、ご用命下さいませ。<br />
					 </dl>
					 </div><!--/.list_article-->
					 
					</div>
					<!--++/.sec++-->

					<!--++.sec/++-->
					<div class="sec order_bloc cf">
					<!--  <form method="post" action="../postmail1/postmail.cgi"> -->
					<!--  <form id="contact-form" method="post" action="../postmail1/postmail.cgi"> -->
					<form id="contact-form" method="post" action="">

					<!-- 必須項目カウントボックス -->
					  <div id="landing_form-wrapper">
					   <div id="required_box">必須項目が<strong id="required_total">&nbsp;&nbsp;項目</strong>あります。<br />
						 <span id="required_remaining">残り<strong>&nbsp;&nbsp;項目</strong></span>
					   </div>
					<!-- 必須項目カウントボックス end -->

					<!-- フォーム上部タイトル画像 -->
					<img src="<?php echo get_template_directory_uri();?>/image/button_banner/input_form_title.png">
					<br><br>


					  <input type="hidden" name="need" value="姓 名 セイ メイ 郵便番号 都道府県 住所 電話番号 email" />
					  <input type="hidden" name="credit" value="" />
					  <table width="600" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#0066FF">
					   <tbody>
						<tr>
						 <td colspan="2" bgcolor="#a9e36c"><strong><font size="2">お支払い方法を選択して下さい</font></strong> </td>
						 </tr>
						<tr>
						 <td align="center" bgcolor="#a9e36c"><input class="payment" name="お支払い方法" type="radio" value="代金引換(一括払い) 高性能ハンドリフト  <?php echo $limited_price;?>（＋送料<?php echo $shipping_charge;?>）" checked="checked" /></td>
						 <td bgcolor="white"><font size="2"><strong>代金引換(一括払い)   <?php echo $limited_price;?></strong>（＋送料<?php echo $shipping_charge;?>）</font></td>
						 </tr>
						<tr>
						 <td width="50" align="center" bgcolor="#a9e36c"><input class="payment" name="お支払い方法" value="銀行振込(一括払い) 高性能ハンドリフト  <?php echo $limited_price;?>（＋送料<?php echo $shipping_charge;?>）" type="radio" /></td>
						 <td bgcolor="white"><font size="2"><strong>銀行振込(一括払い)   <?php echo $limited_price;?></strong>（＋送料<?php echo $shipping_charge;?>）</font></td>
						 </tr>
						<tr>
						 <td width="50" align="center" bgcolor="#a9e36c"><input class="payment credit-card-option" name="お支払い方法" value="クレジット決算　高性能ハンドリフト  <?php echo $limited_price;?>（＋送料<?php echo $shipping_charge;?>）" type="radio" /></td>
						 <td bgcolor="white"><font size="2"><strong>クレジット決算  <?php echo $limited_price;?></strong>（＋送料<?php echo $shipping_charge;?>）</font></td>
						 </tr>
						
						</tbody>
					   </table>
					  <br />
					  <table width="600" border="0" align="center" cellpadding="5" cellspacing="1" class="backimg">
					   <tbody>
						<tr>
						 <td colspan="2" align="left" bgcolor="#a9e36c"><font size="2"><strong>お名前を 入力してください。</strong></font></td>
						 </tr>
						<tr>
						 <td align="left" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span> 姓<div class="hissu">必須</div></td>
						 <td class="backalp" align="left"><font size="2">
						  <input type="text" id="sei" class="validate[required] text-input surname" name="姓" size="17" />
						  <!-- <input type="text" name="姓" size="17" /> -->
						  </font>(例：田中)</td>
						 </tr>
						<tr>
						 <td width="130" align="left" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span> 名<div class="hissu">必須</div></td>
						 <td class="backalp" align="left"><font size="2">
							 <input type="text" id="mei" class="validate[required] text-input name" name="名" size="17" />
							 <!-- <input type="text" name="名" size="17" /> -->
							 </font>(例：太郎)</td>
						 </tr>
						<tr>
						 <td width="130" align="left" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span> セイ<div class="hissu">必須</div></td>
						 <td class="backalp" align="left"><font size="2">
						  <input type="text" id="sei-kana" class="validate[required] text-input say" name="セイ" size="17" />
						  <!-- <input type="text" name="セイ" size="17" /> -->
						  <span class="ft_small"> (例：タナカ)</span></font></td>
						 </tr>
						<tr>
						 <td width="130" align="left" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span> メイ<div class="hissu">必須</div></td>
						 <td class="backalp" align="left"><font size="2">
						  <input type="text" id="mei-kana" class="validate[required] text-input mei" name="メイ" size="17" />
						  <!-- <input type="text" name="メイ" size="17" /> -->
						  <span class="ft_small">(例：タロウ)</span></font></td>
						 </tr>
						</tbody>
					   </table>
					  <br />
					  <table width="600" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#0066FF">
					   <tbody>
						<tr>
						 <td colspan="2" align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="2"><strong>お届け先住所を入力してください。</strong></font></td>
						 </tr>
						<tr>
						<td align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="3"><span class="ft_blue"></span>郵便番号<strong></strong></font><div class="hissu">必須</div></td>
						<td align="left" bgcolor="#FFFFFF"><font size="3"><span class="ft_small">〒</span>
						  <input type="text" id="zipcode1" class="validate[requiredZip,custom[zip1]] text-input postal-1" name="郵便番号" style="ime-mode: inactive;"  size="4" maxlength="3" />
						  - <input type="text" id="zipcode2" class="validate[requiredZip,custom[zip2]] text-input postal-2" name="郵便番号2" onKeyUp="AjaxZip3.zip2addr('郵便番号','郵便番号2','都道府県','住所');"  style="ime-mode: inactive;" size="5" maxlength="4" />
						  <span class="ft_small"> (例：273-0001)</span><br />【<a href="http://www.post.japanpost.jp/zipcode/index.html" target="blank">〒郵便番号検索ページへ】</a></font>
						</td>
						</tr>
						<tr>
						 <td width="130" align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span>都道府県<div class="hissu">必須</div></td>
						 <td align="left" bgcolor="#FFFFFF"><font size="2">
						  <select  id="prefecture" class="validate[required] pref" name="都道府県" size="1">
						  <!-- <select name="都道府県" size="1"> -->
						   <option selected="selected" value="">---都道府県を選択してください---</option>
						   <option value="北海道">北海道</option>
						   <option value="青森県">青森県</option>
						   <option value="秋田県">秋田県</option>
						   <option value="岩手県">岩手県</option>
						   <option value="山形県">山形県</option>
						   <option value="宮城県">宮城県</option>
						   <option value="福島県">福島県</option>
						   <option value="新潟県">新潟県</option>
						   <option value="富山県">富山県</option>
						   <option value="石川県">石川県</option>
						   <option value="群馬県">群馬県</option>
						   <option value="栃木県">栃木県</option>
						   <option value="長野県">長野県</option>
						   <option value="岐阜県">岐阜県</option>
						   <option value="埼玉県">埼玉県</option>
						   <option value="茨城県">茨城県</option>
						   <option value="東京都">東京都</option>
						   <option value="千葉県">千葉県</option>
						   <option value="神奈川県">神奈川県</option>
						   <option value="静岡県">静岡県</option>
						   <option value="山梨県">山梨県</option>
						   <option value="愛知県">愛知県</option>
						   <option value="福井県">福井県</option>
						   <option value="滋賀県">滋賀県</option>
						   <option value="三重県">三重県</option>
						   <option value="京都府">京都府</option>
						   <option value="奈良県">奈良県</option>
						   <option value="兵庫県">兵庫県</option>
						   <option value="大阪府">大阪府</option>
						   <option value="和歌山県">和歌山県</option>
						   <option value="島根県">島根県</option>
						   <option value="鳥取県">鳥取県</option>
						   <option value="岡山県">岡山県</option>
						   <option value="広島県">広島県</option>
						   <option value="山口県">山口県</option>
						   <option value="香川県">香川県</option>
						   <option value="愛媛県">愛媛県</option>
						   <option value="徳島県">徳島県</option>
						   <option value="高知県">高知県</option>
						   <option value="大分県">大分県</option>
						   <option value="福岡県">福岡県</option>
						   <option value="佐賀県">佐賀県</option>
						   <option value="熊本県">熊本県</option>
						   <option value="長崎県">長崎県</option>
						   <option value="宮崎県">宮崎県</option>
						   <option value="鹿児島県">鹿児島県</option>
						   <option value="沖縄県">沖縄県</option>
						   </select>
						  </font> (例：千葉県)</td>
						 </tr>
						<tr>
						 <td width="130" align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span>住所<div class="hissu">必須</div></td>
						 <td align="left" bgcolor="#FFFFFF"><font size="2">
						  <input type="text" id="address" class="validate[required] text-input address" name="住所" size="45" />
						  <!-- <input type="text" name="住所" size="45" /> -->
						  </font><br> (例：船橋市宮本2-7-1)</td>
						 </tr>
						<tr>
						 <td width="130" align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span>建物名</font></td>
						 <td align="left" bgcolor="#FFFFFF"><font size="2">
						  <input type="text" name="建物名" class="building-name" size="45" />
						  </font><br> (例：船橋ビル２F)</td>
						 </tr>
						<tr>
						<td width="130" align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="3"><span class="ft_blue"></span>電話番号<strong></strong></font><div class="hissu">必須</div></td>
						<td align="left" bgcolor="#FFFFFF"><font size="3"><span class="ft_small"> TEL:</span>
						  <input type="text" id="telephone1" class="validate[requiredTel,custom[phone1]] text-input tel-1" name="電話番号" size="4" maxlength="4" style="ime-mode: inactive;" style="background:#ccc;" onkeyup="if($(this).val() != ''){$(this).css('background','#fff');}else{$(this).css('background','#ccc');}"/>
						  - <input type="text" id="telephone2" class="validate[requiredTel,custom[phone2]] text-input tel-2" name="電話番号2" size="4" maxlength="4" style="ime-mode: inactive;" style="background:#ccc;" onkeyup="if($(this).val() != ''){$(this).css('background','#fff');}else{$(this).css('background','#ccc');}" />
						  - <input type="text" id="telephone3" class="validate[requiredTel,custom[phone3]] text-input tel-3" name="電話番号3" size="4" maxlength="4" style="ime-mode: inactive;" style="background:#ccc;" onkeyup="if($(this).val() != ''){$(this).css('background','#fff');}else{$(this).css('background','#ccc');}" />
						  <span class="ft_small">(例：047-407-0431)</span></font></td>
						</tr>
						<tr>
						 <td width="130" align="left" nowrap="nowrap" bgcolor="#a9e36c"><font size="2"><span class="ft_blue"></span>メールアドレス<div class="hissu">必須</div></td>
						 <td align="left" bgcolor="#FFFFFF"><font size="2">
						  <input type="text" id="email" class="validate[required,custom[email]] text-input e-mail" name="email" size="35" style="ime-mode:disabled;" />
						  <!-- <input type="text" name="email" size="35" /> -->
						  <span class="ft_small">　<br />
						  (例：tanaka@dream.com) <br>
					ご記入間違いが多くなっております。 再度、ご確認くださいませ。</span></font></td>
						 </tr>
						 <tr>
							<td colspan="2" bgcolor="#FFFFFF">
								<div style="max-height: 300px;
    overflow-y: auto;
    height: 300px;
    display: block;
    width: 95%;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
本系やUは、株式会社ドリームリンク（以下「弊社」）が運営するすべてのインターネット上の通信販売に関するサービスを利用するお客様と弊社の関係を定めるものです。<br/>
本規約は、弊社にてお客様の了承を得ることなく、本規約を随時更新・追加・部分改廃することができます。また、内容を開示した時点ですべての利用者はこれを承諾したものとします。弊社にてお買い物を楽しまれるお客様におかれましては、この利用規約をご確認ください。<br/>
第一条 納期について<br/>
インターネット上に記載の納期は、商品配までの目安の納期です。何らかの理由によりインターネット上に記載の納期と配送予定日が変動する場合がございます。また、希望到着日のお届けを確約するものではなく、配達指定日に商品のお届けができない場合もあります。<br/>
第二条 商品の欠品について<br/>
商品の在庫がある場合、受注・決済確認後、早々に商品の出荷をいたします。なお、弊社では複数のウェブサイトからのご注文と電話などでのご注文も受け付けている関係でウェブサイトでご注文が完了したのちに欠品扱いとなる場合もございます。<br/>
第三条 掲載内容について<br/>
弊社ウェブサイト上のあらゆる情報につきましては、最新の注意を払い正確に保つよう努めております。但し、掲載されているあらゆる情報（商品仕様・商品画像・文言・商品価格・その他）が正しいことを保証するものではありません。<br/>
第四条　商品価格の変動について<br/>
販売価格は、弊社の価格調整や在庫数により変動する可能性があります。注文後に、販売価格に変更がありましても価格調整や差額の返金等は致しかねます。また、弊社ウェブサイトの店舗間で同一商品を異なる販売価格で提供することがありますが、ご注文時の表示価格での販売となり価格調整や差額の返金等は行いません。<br/>
第四条 商品不良・誤配送ついて<br/>
第1項 商品不良について<br/>
商品不良が発生した場合は、弊社が別途定める「初期不良・交換・返品」の範囲内で不良商品の対応を行います。なお、商品不良のため、お客様が弊社でなく他で代品を用意された場合等の、それにかかる費用および精神的、時間的、その他すべての保証を弊社は行いません。<br/>
第2項 誤配送について<br/>
商品誤配送の場合は、注文商品との交換にて対応いたします。なお、誤配送の為、お客様が弊社でなくほかで代品を用意された場合等の、それにかかる費用および精神的、時間的、その他すべての保証を弊社は行いません。<br/>
第六条　保障について<br/>
第1項 保障の可否判断に関して<br/>
保証の判断はすべて弊社およびメーカーにて行うものとします。下記の場合は保証の対象外となります。<br/>
・商品販売ページに保証についての記載がない場合。<br/>
・お客様の誤使用、または、改造や過失、不当な修理による故障と判断した場合。<br/>
・地震、火災、落雷、風水害、公害などの外的要因により故障した場合<br/>
・消耗品の部品およびパーツ類<br/>
・弊社およびメーカーが保証摘要外と判断した場合。<br/>
第2項 保証内容に関して<br/>
ウェブサイト上に記載の「保証」とは、保証期間内の返品又は、同商品との交換をするものではなく、保証期間内の不良部分の該当部品の提供、修復方法のご案内を無償にて行うものとなります。商品の交換又は返品は、弊社が別途ただメル「初期不良・交換・返品」の範囲内での対応となります。<br/>
第七条　交換と返品について<br/>
第1項 交換・返品の対応期間について<br/>
商品の交換・返品は商品到着後7日以内に限りお申込みいただけます。7日間を経過してからの交換・返品はお受けいたしかねます。いかなる場合でも、改造・加工後の「返品」や「クレーム」、設置費や施工費、修理費等の負担等については一切お受けできませんので予めご了承下さい。<br/>
第2項 交換について<br/>
商品の良品・不良品の判断は、弊社又はメーカーにて行うものとします。<br/>
交換する商品の在庫がない場合、入荷次第、すみやかに配送をさせていただきます。入荷の遅延、交換商品の欠品に伴い発生したすべての損害につきましては弊社では一切の責任を負いません。<br/>
第3項 返品・返金に関して<br/>
商品の良品・不良品の判断は、弊社又はメーカーにて行うものとします。<br/>
お客様都合による返品・交換は、未開封、未使用のものに限らせていただきます。開封後、ご使用後のお客様都合による返品はお受けでき欠けるためご了承ください。<br/>
返金手続きは、弊社指定倉庫に返品商品が届いたことを確認した後となります。<br/>
返金の場合は注文時のお支払い方法により異なります。原則としてクレジットカード決済を除くほかの決済方法では、銀行振り込みにてご返金をさせていただきます。また現金書留等のほかの方法でのご返金は承っておりません。またご返金金額は振込手数料（ご返金額によって異なります）及び、ご購入時の送料手数料（送料、代引き手数料、梱包手数料、銀行振込時の振込手数料）を差し引いた金額となります。商品の初期不良・破損の返品（弊社都合による返品）の場合、商品返送時の送料は弊社負担とさせていただき、別途お知らせさせていただく住所までご返送いただくことになります。<br/>
送料無料の商品にも、弊社負担する送料が発生しております。お客様都合の返品・キャンセル・交換の際には、実費を頂戴いたします。<br/>
第4項 免責事項<br/>
商品の利用又は、不良によって発生したいかなる損害（事業利益の損失、作業の中断、金銭的損害を含む）に関しましても弊社では一切責任を負わないものとします。<br/>
第八条　免責事項について<br/>
弊社は、提供するすべての商品、サービス、情報に際し、お客様に対して発生したいかなる損害や事故等に対して、一切の責任を負わないものとします。<br/>
また、弊社はお客様が第三者に与えたいかなる種類の損害や事故等に対しても一切の責任を負わないものとします。お客様が第三者に与えた損害や事故等はお客様の責任を費用をもって解決するものとします。<br/>
第九条　準拠法と管轄裁判所について<br/>
弊社ウェブサイトでのインターネット上の通信販売や本利用規約の運用は、日本法に準拠します。またすべての紛争に関し千葉地方裁判所を第一審の管轄裁判所とします。<br/>
平成27年1月1日更新
								</div>
								<div style="text-align:center;margin-top:10px;">
									<font size="2">
										<input type="checkbox" id="agree" class="validate[required] agree" name="agree" size="35"/> <span class="agree-text">同意する<span>
									</font>
								</div>
							</td>
						 </tr>
						</tbody>
					   </table>
					  <br />

					   <!-- 送信ボタン処理 -->
					   <div id="confirm_button" align="center" class="marginU60">
						 <div id="flash_confirm" style="position:relative;">

					<!-- 送信ボタン音処理 -->
					<audio id="overSound" preload="auto">
						<source src="<?php echo get_template_directory_uri();?>/sound/meka_ge_mouse_s01.mp3" type="audio/mp3">
						<p>※お使いのブラウザはHTML5のaudio要素をサポートしていないので音は鳴りません。</p>
					</audio>
					<!-- 送信ボタン音処理ここまで -->

							<span style="position:absolute;top:0;left:0;text-align:center;width:100%;">
							<div id="impt_back" class="animated2 infinite bounce" style="width:466px;height:95px;margin:auto;"></div>
							<div style="position:absolute;top:0;left:116px;z-index:10;">
						  <!--<input type="image" id="impt" class="animated2 infinite bounce2 appButton" src="<?php echo get_template_directory_uri();?>/dream/img/contact_btn_text.png" alt="お申し込み内容確認" onclick="itemOrder(39850);javascript:conf();"./>-->
						
							<a href="#order_details" class="submit_order"><img id="impt" class="animated2 infinite bounce2 appButton" src="<?php echo get_template_directory_uri();?>/dream/img/contact_btn_text.png" alt="お申し込み内容確認" ></a>
						</div>
						</span>
						 </div>
					   </div>   
					  
					  <div id="confirm_button_disable" align="center" class="marginU60">
						<div>
						<input type="image" id="impt" src="<?php echo get_template_directory_uri();?>/dream/img/form_button_disable.png" width="370" height="94" alt="お申し込み内容確認" />
						</div>
					  </div>
					  </form>
					</div>
					<!--++/.sec++-->




					<!--++.sec/++-->
					<div class="sec cf">
					<ul class="btm_contact">
					<!-- <li><a href="../tel.html"><img src="../common/img/tel_banner_off.gif" alt="お電話でのお見積り、ご相談はこちら" /></a></li> -->
					</ul>
					</div>
					<!--++/.sec++-->

	            </div>
	            <!--/.main-->
	            <!--++/.main++-->
				
				<!-- Modal HTML embedded directly into document -->
				<div id="order_details" style="display:none;">
					<div class="header">
						<h1>お問い合わせフォーム > 確認画面</h1>
						<h2>以下の内容でよろしければ「送信する」ボタンを押して下さい。</h2>
					</div>
					<table>
						<tr><td>お支払い方法</td><td class="pay-method"></td></tr>
						<tr><td>姓</td><td class="txt-surname"></td></tr>
						<tr><td>名</td><td class="txt-name"></td></tr>
						<tr><td>セイ</td><td class="txt-say"></td></tr>
						<tr><td>メイ</td><td class="txt-mei"></td></tr>
						<tr><td>郵便番号</td><td class="txt-postal-code"></td></tr>
						<tr><td>都道府県</td><td class="txt-prefectures"></td></tr>
						<tr><td>住所</td><td class="txt-address"></td></tr>
						<tr><td>建物名</td><td class="txt-building-name"></td></tr>
						<tr><td>電話番号</td><td class="txt-telephone-number"></td></tr>
						<tr><td>メールアドレス</td><td class="txt-email"></td></tr>
					</table>
					<a class="btn-back" href="javascript:void(0)" rel="modal:close">< 前に戻る</a>
					<a class="btn-final-submit" href="javascript:void(0)" >送信する ></a>
				</div>
				
				<!-- hidden form for Credit Card payment-->
				<FORM METHOD="POST" ACTION="https://linkpt.cardservice.co.jp/cgi-bin/credit/order.cgi" id="formCreditCard" TARGET="_top" style="display:none;"> 
				<INPUT TYPE="hidden" NAME="clientip" VALUE="2019000801">
				<?php 
					$limited_price_val =str_replace(",","",$limited_price_val);
					$limited_price_val =str_replace("円","",$limited_price_val);
					$price = floatval(floatval($limited_price_val) + intval(3000));
				?>
				<INPUT TYPE="hidden" NAME="money" value="<?php echo $price;?>">
				<INPUT TYPE="hidden" NAME="sendid" class="credit_sendid" VALUE="">
				<INPUT TYPE="hidden" NAME="telno" class="credit_tell_no" VALUE="0334989030">
				<INPUT TYPE="hidden" NAME="email" class="credit_email" VALUE="*****@cardservice.co.jp">
				<INPUT TYPE="hidden" NAME="success_url" VALUE="<?php echo get_site_url();?>/thank-you-message/">
				<INPUT TYPE="hidden" NAME="success_str" VALUE="トップページへ">
				<INPUT TYPE="hidden" NAME="failure_url" VALUE="<?php echo get_site_url();?>"> 
				<INPUT TYPE="hidden" NAME="failure_str" VALUE="トップページへ">
				<INPUT TYPE="submit" VALUE="クレジット決済ページ">
				</FORM>
	        </div>
	        <!--+++/.container+++-->

			
<?php 

	get_footer(); 
?>