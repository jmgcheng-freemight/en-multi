<?php
/**
 * Template Name: apistockupdate
 *
 * @package Hand-lift 1.0
 * @since Hand-lift 1.0
 */
 ?>

<?php
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Allow-Headers: origin, content-type, accept');
	

	/**/
	$i_post_id = 0;
	$s_post_permalink = '';
	$a_result = array();
	$i_new_qty = 0;
	$i_new_mod = 0;
	

	/**/
	if( 	isset($_GET['StoreAccount']) && !empty($_GET['StoreAccount']) 
		&& 	isset($_GET['Code']) && !empty($_GET['Code']) 
		&& 	isset($_GET['Stock']) 
	)
	{
		$i_new_qty = $_GET['Stock'];
	}
	else
	{
		wp_redirect( home_url() ); 
		exit();
	}
	
	
	/**/
	if( !is_numeric($i_new_qty) )
	{
		wp_redirect( home_url() ); 
		exit();
	}
	
	
	/**/
	if( $i_new_qty < 0 )
	{
		wp_redirect( home_url() ); 
		exit();
	}
	
	
	/*
	$i_new_mod = $i_new_qty % 1;
	echo $i_new_mod;
	if( $i_new_mod != 0 )
	{
		wp_redirect( home_url() ); 
		exit();
	}
	*/
	


	/**/
	$a_args = array(
		'post_type'  		=> 'products',
		'meta_key'   		=> 'product_number',
		'posts_per_page' 	=> 1,
		'meta_query' => array(
			array(
				'key'     => 'product_number',
				'value'   => $_GET['Code']
			),
		),
	);
	$o_query = new WP_Query( $a_args );
	if ( $o_query->have_posts() ) 
	{
			$o_query->the_post();
			$i_post_id = get_the_ID();
			$s_post_permalink = get_permalink($i_post_id);
	}
	else
	{
		wp_redirect( home_url() ); 
		exit();	
	}
	wp_reset_postdata();


	/**/
	if( isset($i_post_id) && !empty($i_post_id) )
	{
	
		/**/
		update_post_meta($i_post_id, 'av_stock', $i_new_qty);

		/*
		wp_redirect( $s_post_permalink );
		*/

		echo 'stock ' . $_GET['Code'] . ' updated qty to ' . $_GET['Stock'];
		exit();
	}
	else
	{
		wp_redirect( home_url() );
		exit();	
	}
?>