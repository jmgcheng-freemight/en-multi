<?php
/**
 * Template Name: CGI Return
 *
 * @package Hand-lift 1.0
 * @since Hand-lift 1.0
 */
 ?>
<?php 	
	global $wpdb;
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}	
	$now = date('Y/m/d H:i:s');
	//if($ip == '210.164.6.67' || $ip == '202.221.139.50'){
		$sendid = $_GET['sendid'];
		$result     = $_GET['result'];
		$email     = $_GET['email'];
		if($result == 'ok' || $result == 'Ok' || $result == 'OK'){
			send_handlift_mail($sendid,$email);
		}
	//}
?>