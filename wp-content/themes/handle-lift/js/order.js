$("#fade").modal({
  fadeDuration: 100
});

$(document).ready(function() {
	var credit_card;
	if($('.upsell_link').length){
		function order_details() {		
			this.sendid = $('.order-sendid').val();
			this.product_id = $('.pid').val();
			this.product_code = $('.pcode').val();
			this.product_name = $('.pname').val();
			
			this.surname = $('.surname').val();
			this.name =$('.name').val();
			this.say =$('.say').val();
			this.mei =$('.mei').val();
			this.postal_code =$('.postal-1').val() +'-'+ $('.postal-2').val(); 
			this.pref =$('#prefecture').val();
			this.address =$('.address').val(); 
			this.building_name=$('.building-name').val();
			this.telephone_number=$('.tel-1').val() +'-'+ $('.tel-2').val() +'-'+ $('.tel-3').val(); 
			this.email=$('.e-mail').val();
			
			this.item_amount=$('.amount').val();
			this.payment_method = $('.payment:checked').val();
			this.shipping_charge=$('.shipping').val();
			this.delivery_method=$('.delivery').val();
			
		}
	} else{
		function order_details() {		
			this.sendid = $('.order-sendid').val();
			this.product_id = $('.order-pid').val();
			this.product_code = $('.order-pcode').val();
			this.product_name = $('.order-pname').val();
			
			this.surname = $('.order-surname').val();
			this.name =$('.order-name').val();
			this.say =$('.order-say').val();
			this.mei =$('.order-mei').val();
			this.postal_code =$('.order-postal-code').val(); 
			this.pref =$('.order-prefecture').val();
			this.address =$('.order-address').val(); 
			this.building_name=$('.order-building-name').val();
			this.telephone_number=$('.order-tell-no').val(); 
			this.email=$('.order-e-mail').val();
			
			this.item_amount=$('.order-amount').val();
			this.payment_method = $('.order-payment').val();
			this.shipping_charge=$('.order-shipping').val();
			this.delivery_method=$('.order-delivery').val();			
		}
		var str = $('.order-payment').val();
		if (str.indexOf('クレジット') >= 0){
			$('.upsell-submit').removeAttr('rel');
			credit_card = true;
		} else {
			$('.upsell-submit').attr('rel', 'modal:open');
			credit_card = false;
		}
	}
	$( document ).on( 'click', '.upsell-submit', function(event) {
		if(credit_card){
			$('#formCreditCard').submit();
			return false;
		}
	});
	
	
	$( document ).on( 'click', '.submit_order', function(event) {
		
		var order = new order_details();
		
		$('.pay-method').text(order.payment_method);
		$('.txt-surname').text(order.surname);
		$('.txt-name').text(order.name);
		$('.txt-say').text(order.say);
		$('.txt-mei').text(order.mei);
		$('.txt-postal-code').text(order.postal_code);
		$('.txt-prefectures').text(order.pref);
		$('.txt-address').text(order.address);
		$('.txt-building-name').text(order.building_name);
		$('.txt-telephone-number').text(order.telephone_number);
		$('.txt-email').text(order.email);
		var phone = $('.tel-1').val() +''+ $('.tel-2').val() +''+ $('.tel-3').val(); 
		var email = $('.e-mail').val();
		$('.credit_tell_no').val(phone);
		$('.credit_email').val(email);
		$.ajax({
			dataType: "json",
			url: ajaxurl,
			type: "POST",
			data: {
				'action': 'save_order',
				'order': order
			},
			success:function(response) {
				console.log(response);
				if(response.status == 'Success'){
					$('.order-sendid').val(response.sendid);
					if($('.credit-card-option').is(':checked')){
						$('.credit_sendid').val(response.sendid);
						if($('.upsell_link').val()){
							var upsell_link = $('.upsell_link').val();
							window.location.href = upsell_link+'?key='+response.sendid;
						} else{
							$('#formCreditCard').submit();	
						}						
					} else {
						if($('.upsell_link').val()){
							var upsell_link = $('.upsell_link').val();
							window.location.href = upsell_link+'?key='+response.sendid;
						}
					}
				}										
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
		
	});
	if ($('input.credit-card-option').is(':checked')) {
		$('.submit_order').removeAttr('rel');
	} else {
		if(!$('.upsell_link').val()){
			$('.submit_order').attr('rel', 'modal:open');
		}
	}
	
	$( document ).on( 'submit', '#contact-form', function() {
		if($("#agree").prop('checked')){
			return true;
		} else{
			var pos = $(".agree-text").position();
			var top =parseInt(pos.top) + 16;
			var left = parseInt(pos.left)-16;
			$("#agree").parent().append('<div class="emailformError parentFormcontact-form formError" style="opacity: 0.87; position: absolute; top: '+top+'px; left: '+left+'px; margin-top: -68px; display: block;"><div class="formErrorContent"><span class="close">×</span>* 必須項目です<br><span class="close">×</span>*上記内容に同意されていません。<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>');
			return false;
		};
		
	});
	$( document ).on( 'click', '#agree_upsell', function(event) {
		var price = parseInt($('.upsell-price').val().replace(/,/g,''));
		var amount = parseInt($('.order-amount').val());
		var total_amount = parseInt($('.order-credit-amount').val());
		var sendid = $('.order-sendid').val();
		var original_amount = $('.order-original-amount').val();
		var shipping_cost = $('.order-shipping').val();
		var data = new Array();
		var upsell_amount = 0;
		var upsell_name = null;
		var upsell_number = null;
		if($(this).is(':checked')){
			var new_amount = parseInt(amount) + parseInt(price);
			$('.order-amount').val(new_amount);
			var new_credit_amount = parseInt(total_amount + price);
			$('.order-credit-amount').val(new_credit_amount);
			$('.upsell-tr').removeAttr('style');
			upsell_amount = price;
			upsell_name = $('.upsell-name').val();
			upsell_number = $('.upsell-number').val();
		} else {
			var new_amount = parseInt(amount) - parseInt(price);
			$('.order-amount').val(new_amount);
			var new_credit_amount = parseInt(total_amount - price);
			$('.order-credit-amount').val(new_credit_amount);
			$('.upsell-tr').css('display', 'none');
			upsell_amount = 0;
		}
		var data = [];
		data = {
				'sendid':sendid,
				'amount':original_amount,
				'shipping_cost':shipping_cost,
				'upsell_amount':upsell_amount,
				'upsell_name':upsell_name,
				'upsell_number':upsell_number,
			};
		
		console.log(data);
		$.ajax({
			url: ajaxurl,
			type: "POST",
			data: {
				'action': 'update_order',
				'data': data
			},
			success:function(response) {
				console.log(response);
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
	});
	
	$( document ).on( 'change', '.payment', function() {
		if($(this).hasClass('credit-card-option')){
			$('.submit_order').removeAttr('rel');
		} else {
			if(!$('.upsell_link').val()){
				$('.submit_order').attr('rel', 'modal:open');
			}
		}
	});
	$( document ).on( 'click', '.btn-final-submit', function(event) {
		var order = new order_details();
		$('.jquery-modal').hide();
		$.ajax({
			url: ajaxurl,
			type: "POST",
			data: {
				'action': 'send_order',
				'order': order
			},
			success:function(response) {
				console.log(response);
				
				window.location.href="/thank-you-message/";
				
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
		
	});
	
});