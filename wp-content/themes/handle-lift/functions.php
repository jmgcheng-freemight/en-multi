<?php
/**
 * Handle Lift functions and definitions
 *
 */

define("TEMPALTE_DIRECTORY_URI", get_template_directory_uri());
define("BLOG_URL", get_site_url()); 
 
 
/* update_option('siteurl','hl.dev'); 
update_option('home','hl.dev');
 */
function handlelift_setup() {

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	/*add_theme_support( 'title-tag' );*/


	/*
		create product post type
	*/
	$a_labels = array(
		'name'                => _x( 'Products', 'Post Type General Name' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Products' ),
		'parent_item_colon'   => __( 'Parent Product' ),
		'all_items'           => __( 'All Products' ),
		'view_item'           => __( 'View Product' ),
		'add_new_item'        => __( 'Add New Product' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Product' ),
		'update_item'         => __( 'Update Product' ),
		'search_items'        => __( 'Search Product' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'Products' ),
		'description'         => __( 'Products' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
		'menu_position'      => 5
	);
	register_post_type( 'products', $a_args );
	
	
	/*
		create up sell post type
	*/
	$a_labels = array(
		'name'                => _x( 'Up Sells', 'Post Type General Name' ),
		'singular_name'       => _x( 'Up Sell', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Up Sells' ),
		'parent_item_colon'   => __( 'Parent Up Sell' ),
		'all_items'           => __( 'All Up Sells' ),
		'view_item'           => __( 'View Up Sell' ),
		'add_new_item'        => __( 'Add New Up Sell' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Up Sell' ),
		'update_item'         => __( 'Update Up Sell' ),
		'search_items'        => __( 'Search Up Sell' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'Up Sells' ),
		'description'         => __( 'Up Sells' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'supports'           => array( 'title', 'editor', 'thumbnail'  ),
		'menu_position'      => 5
	);
	register_post_type( 'upsell', $a_args );


	/*
		create useguide post type
	*/
	$a_labels = array(
		'name'                => _x( 'Useguides', 'Post Type General Name' ),
		'singular_name'       => _x( 'Useguide', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Useguides' ),
		'parent_item_colon'   => __( 'Parent Useguide' ),
		'all_items'           => __( 'All Useguides' ),
		'view_item'           => __( 'View Useguide' ),
		'add_new_item'        => __( 'Add New Useguide' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Useguide' ),
		'update_item'         => __( 'Update Useguide' ),
		'search_items'        => __( 'Search Useguide' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'Useguides' ),
		'description'         => __( 'Useguides' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'useguide', $a_args );


	/*
		create handlift post type
	*/
	$a_labels = array(
		'name'                => _x( 'User Datas', 'Post Type General Name' ),
		'singular_name'       => _x( 'User Data', 'Post Type Singular Name' ),
		'menu_name'           => __( 'User Datas' ),
		'parent_item_colon'   => __( 'Parent User Data' ),
		'all_items'           => __( 'All User Datas' ),
		'view_item'           => __( 'View User Data' ),
		'add_new_item'        => __( 'Add New User Data' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit User Data' ),
		'update_item'         => __( 'Update User Data' ),
		'search_items'        => __( 'Search User Data' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'User Datas' ),
		'description'         => __( 'User Datas' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'user-data', $a_args ); //old site use user_data, just following that..
	
	/*
		create handlift orders post type jb*
	*/
	$o_labels = array(
		'name'                => _x( 'Orders', 'Post Type General Name' ),
		'singular_name'       => _x( 'Order', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Orders' ),
		'parent_item_colon'   => __( 'Parent Orders' ),
		'all_items'           => __( 'All Orders' ),
		'view_item'           => __( 'View Orders' ),
		'add_new_item'        => __( 'Add New Orders' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Orders' ),
		'update_item'         => __( 'Update Orders' ),
		'search_items'        => __( 'Search Orders' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$o_args = array(
		'label'               => __( 'Orders' ),
		'description'         => __( 'Orders' ),
		'labels'              => $o_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => false,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'orders', $o_args ); //old site use user_data, just following that..
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'handlelift_setup' );

	
/*function filter_function_name($title) {
	return $title;
}
add_filter( 'wp_title', 'filter_function_name', 10, 2 );*/


remove_filter( 'the_content', 'wpautop' );


function set_content_url($s_content)
{
	$s_content = str_replace('{{TEMPALTE_DIRECTORY_URI}}', TEMPALTE_DIRECTORY_URI, $s_content);
	$s_content = str_replace('{{BLOG_URL}}', BLOG_URL, $s_content);
	return $s_content;
}
add_filter('the_content','set_content_url'); 






function no_stock_notice_func() {
	$i_qty_stock_left = 0;
	$i_post_id = get_the_ID();

	if( isset($i_post_id) && !empty($i_post_id) )
	{
		$i_qty_stock_left = get_post_meta( $i_post_id, 'av_stock', true ) ;
	}

	if( $i_qty_stock_left <= 0 )
	{
		$s_notice = '<div class="product-notice product-notice-noqty"><p>現在在庫がございません</p></div>';
	}

	return $s_notice;
	
	
}
add_shortcode( 'no_stock_notice', 'no_stock_notice_func' );




function product_order_btn_func() {
	$i_qty_stock_left = 0;
	$i_post_id = get_the_ID();

	if( isset($i_post_id) && !empty($i_post_id) )
	{
		$i_qty_stock_left = get_post_meta( $i_post_id, 'av_stock', true ) ;
	}

	if( $i_qty_stock_left <= 0 )
	{
		$s_btn = '<div class="product-notice product-notice-noqty"><p>現在在庫がございません</p></div>';
	}
	else
	{
		$s_btn = '<a href="' .BLOG_URL. '/order-form?pid=' .$i_post_id. '" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage(\'toiawase21\',\'\',\'' . BLOG_URL . '/wp-content/themes/handle-lift/image/products/btn_tyumon01_on.jpg\',1)"><img src="' .BLOG_URL. '/wp-content/themes/handle-lift/image/products/btn_tyumon01.jpg" alt="高性能ハンドリフトご用命はこちら" name="toiawase21" width="577" height="166" border="0" id="toiawase21" /></a>';
	}

	return $s_btn;
}
add_shortcode( 'product_order_btn', 'product_order_btn_func' );



function wpdocs_enqueue_custom_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/common/css/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );

// Add metabox for producst 
add_action( 'save_post', 'product_meta_box_save' );
add_action( 'add_meta_boxes', 'product_meta_box_add' ); 

function product_meta_box_add(){
        add_meta_box( 'predefined_field', 'Product Details', 'product_meta_box_html', 'products', 'side', '' );
}

function product_meta_box_save( $post_id ){

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'product_meta_box_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
	
	$product_number_value = isset($_POST['product_number_field']) ? $_POST['product_number_field'] : '';
	$product_name_field = isset($_POST['product_name_field']) ? $_POST['product_name_field'] : '';
	$load_capacity_value = isset($_POST['load_capacity_field']) ? $_POST['load_capacity_field'] : '';
	$max_height_value = isset($_POST['max_height_field']) ? $_POST['max_height_field'] : '';
	$min_height_value = isset($_POST['min_height_field']) ? $_POST['min_height_field'] : '';
	$weight_value = isset($_POST['weight_field']) ? $_POST['weight_field'] : '';
	$shipping_value = isset($_POST['shipping_field']) ? $_POST['shipping_field'] : '';
	$original_price_value = isset($_POST['original_price_field']) ? $_POST['original_price_field'] : '';
	$comment_value = isset($_POST['comment_field']) ? $_POST['comment_field'] : '';
	$limited_price_value = isset($_POST['limited_price_field']) ? $_POST['limited_price_field'] : '';
	$av_stock_value = isset($_POST['av_stock_field']) ? $_POST['av_stock_field'] : '';
	$upsell_item_number_value = isset($_POST['upsell_item_number_field']) ? $_POST['upsell_item_number_field'] : '';
	
	$product_number_value = sanitize_text_field($product_number_value);
	$product_name_field = sanitize_text_field($product_name_field);
	$load_capacity_value = sanitize_text_field($load_capacity_value); 
	$max_height_value = sanitize_text_field($max_height_value);
	$min_height_value = sanitize_text_field($min_height_value);
	$weight_value = sanitize_text_field($weight_value);
	$shipping_value = sanitize_text_field($shipping_value);
	$original_price_value = sanitize_text_field($original_price_value);
	$comment_value = sanitize_text_field($comment_value);
	$limited_price_value = sanitize_text_field($limited_price_value);
	$av_stock_value = sanitize_text_field($av_stock_value);
	$upsell_item_number_value = sanitize_text_field($upsell_item_number_value);
	
    if( $product_number_value || $load_capacity_value || $max_height_value ){
		update_post_meta($post_id,'product_number',$product_number_value);
		update_post_meta($post_id,'product_name',$product_name_field);
		update_post_meta($post_id,'load_capacity',$load_capacity_value);
		update_post_meta($post_id,'max_height',$max_height_value);
		update_post_meta($post_id,'min_height',$min_height_value);
		update_post_meta($post_id,'weight',$weight_value);
		update_post_meta($post_id,'shipping',$shipping_value);
		update_post_meta($post_id,'original_price',$original_price_value);
		update_post_meta($post_id,'comment',$comment_value);
		update_post_meta($post_id,'limited_price',$limited_price_value);
		update_post_meta($post_id,'av_stock',$av_stock_value);
		update_post_meta($post_id,'upsell_item_number',$upsell_item_number_value);
	}
	
}

function product_meta_box_html( $post ){
    wp_nonce_field( 'product_meta_box_nonce', 'meta_box_nonce' );
	
    //if you know it is not an array, use true as the third parameter
    $product_number_value = get_post_meta($post->ID,'product_number',true);
    $product_name_field = get_post_meta($post->ID,'product_name',true);
    $load_capacity_value = get_post_meta($post->ID,'load_capacity',true);
    $max_height_value = get_post_meta($post->ID,'max_height',true);
    $min_height_value = get_post_meta($post->ID,'min_height',true);
    $weight_value = get_post_meta($post->ID,'weight',true);
    $shipping_value = get_post_meta($post->ID,'shipping',true);
    $original_price_value = get_post_meta($post->ID,'original_price',true);
    $limited_price_value = get_post_meta($post->ID,'limited_price',true);
    $av_stock_value = get_post_meta($post->ID,'av_stock',true);
    $sold_units = get_post_meta($post->ID,'sold_units',true);
    $comment_value = get_post_meta($post->ID,'comment',true);
    $upsell_item_number_value = get_post_meta($post->ID,'upsell_item_number',true);

    ?>
		<label class="meta-label" for="product_number_field">商品番号</label><input name="product_number_field" id="product_number_field" type="text"  value="<?php echo $product_number_value; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="product_name_field">注文商品名</label><input name="product_name_field" id="product_number_field" type="text"  value="<?php echo $product_name_field; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="load_capacity_field">耐荷重</label><input name="load_capacity_field" id="load_capacity_field" type="text"  value="<?php echo $load_capacity_value; ?>" class="mws-textinput" /><span>kg</span><br/>
		<label class="meta-label" for="max_height_field">最高位</label><input name="max_height_field" id="max_height_field" type="text"  value="<?php echo $max_height_value; ?>" class="mws-textinput" /><span>mm</span><br/>
		<label class="meta-label" for="min_height_field">最低位</label><input name="min_height_field" id="min_height_field" type="text"  value="<?php echo $min_height_value; ?>" class="mws-textinput" /><span>mm</span><br/>
		<label class="meta-label" for="weight_field">重　量</label><input name="weight_field" id="weight_field" type="text"  value="<?php echo $weight_value; ?>" class="mws-textinput" /><span>kg</span><br/>
		<label class="meta-label" for="shipping_field">送　料</label><input name="shipping_field" id="shipping_field" type="text"  value="<?php echo $shipping_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="original_price_field">通常価格</label><input name="original_price_field" id="original_price_field" type="text"  value="<?php echo $original_price_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="limited_price_field">限定価格</label><input name="limited_price_field" id="limited_price_field" type="text"  value="<?php echo $limited_price_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="av_stock_field">在庫</label><input name="av_stock_field" id="av_stock_field" type="text"  value="<?php echo $av_stock_value; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="av_stock_field">個数: </label><span><?php echo $sold_units;?></span><br>
		<label class="meta-label" for="comment_field">限定価格</label><textarea name="comment_field" id="comment_field" class="mws-textinput" /><?php echo $comment_value; ?></textarea><br/>
		<label class="meta-label" for="upsell_item_number_field">アップセル商品番号</label><input name="upsell_item_number_field" id="upsell_item_number_field" type="text"  value="<?php echo $upsell_item_number_value; ?>" class="mws-textinput" /><br/>
	<?php	

}


// Add metabox for upsell 
add_action( 'save_post', 'upsell_meta_box_save' );
add_action( 'add_meta_boxes', 'upsell_meta_box_add' ); 

function upsell_meta_box_add(){
        add_meta_box( 'predefined_field', 'Up Sell Details', 'upsell_meta_box_html', 'upsell', 'side', '' );
}

function upsell_meta_box_save( $post_id ){

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'upsell_meta_box_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
	
	$upsell_number_value = isset($_POST['upsell_number_field']) ? $_POST['upsell_number_field'] : '';
	$upsell_product_name_value = isset($_POST['upsell_product_name_field']) ? $_POST['upsell_product_name_field'] : '';
	$original_price_value = isset($_POST['original_price_field']) ? $_POST['original_price_field'] : '';
	$promo_price_value = isset($_POST['promo_price_field']) ? $_POST['promo_price_field'] : '';
	$additional_message_value = isset($_POST['additional_message_field']) ? $_POST['additional_message_field'] : '';
	$agree_message_value = isset($_POST['agree_message_field']) ? $_POST['agree_message_field'] : '';
	
	$upsell_number_value = sanitize_text_field($upsell_number_value);
	$upsell_product_name_value = sanitize_text_field($upsell_product_name_value);
	$original_price_value = sanitize_text_field($original_price_value);
	$promo_price_value = sanitize_text_field($promo_price_value); 
	$additional_message_value = sanitize_text_field($additional_message_value);
	$agree_message_value = sanitize_text_field($agree_message_value);
	
    if( $upsell_product_name_value || $original_price_value || $promo_price_value ){
		update_post_meta($post_id,'upsell_number',$upsell_number_value);
		update_post_meta($post_id,'upsell_product_name',$upsell_product_name_value);
		update_post_meta($post_id,'original_price',$original_price_value);
		update_post_meta($post_id,'promo_price',$promo_price_value);
		update_post_meta($post_id,'additional_message',$additional_message_value);
		update_post_meta($post_id,'agree_message',$agree_message_value);
	}
	
}

function upsell_meta_box_html( $post ){
    wp_nonce_field( 'upsell_meta_box_nonce', 'meta_box_nonce' );
	
    //if you know it is not an array, use true as the third parameter
    $upsell_number_value = get_post_meta($post->ID,'upsell_number',true);
    $upsell_product_name_value = get_post_meta($post->ID,'upsell_product_name',true);
    $original_price_value = get_post_meta($post->ID,'original_price',true);
    $promo_price_value = get_post_meta($post->ID,'promo_price',true);
    $additional_message_value = get_post_meta($post->ID,'additional_message',true);
    $agree_message_value = get_post_meta($post->ID,'agree_message',true);

    ?>
		<label class="meta-label" for="upsell_number_field">商品番号</label><input name="upsell_number_field" id="upsell_number_field" type="text"  value="<?php echo $upsell_number_value; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="upsell_product_name_field">商品名</label><input name="upsell_product_name_field" id="upsell_product_name_field" type="text"  value="<?php echo $upsell_product_name_value; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="original_price_field">通常価格</label><input name="original_price_field" id="original_price_field" type="text"  value="<?php echo $original_price_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="promo_price_field">限定価格</label><input name="promo_price_field" id="promo_price_field" type="text"  value="<?php echo $promo_price_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="additional_message_field">追加メッセージ</label><textarea name="additional_message_field" id="additional_message_field" class="mws-textinput" style="width:100%;height:150px;" /><?php echo $additional_message_value; ?></textarea><br/>
		<label class="meta-label" for="agree_message_field">訴求メッセージ</label><textarea name="agree_message_field" id="agree_message_field" class="mws-textinput" style="width:100%;height:80px;" /><?php echo $agree_message_value; ?></textarea><br/>
	<?php	

}

if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Secondary Image',
            'id' => 'secondary-image',
            'post_type' => 'upsell'
        )
    );
}

//custom functions 
include_once( get_stylesheet_directory() . '/includes/custom-function.php' );
include_once( get_theme_root().'/common-functions.php');