<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />

      <script src="<?php echo get_template_directory_uri();?>/js/jquery-1.11.2.js" type="text/javascript"></script>

      <?php wp_head(); ?>

      <title>
        <?php 
          if ( is_front_page() )
          {
            bloginfo('name');
          }
          elseif( is_archive() )
          {
            $s_slug = get_post_type();
            if( isset($s_slug) && !empty($s_slug) )
            {
              $a_args = array(
                'post_type' => 'page',
                'pagename' => $s_slug
              );
              $o_custom_query = new WP_Query( $a_args );
              if ( $o_custom_query->have_posts() ) 
              {
                  $o_custom_query->the_post();
                  echo get_the_title();
              }
              wp_reset_postdata();
            }
            else
            {
              wp_title('', true);     
            }
          }
          else
          {
            wp_title('', true); 
          }
        ?>
      </title>
      

   </head>
   <body>

    <div id="js-container-whole" class="">

      <div class="l-header">
        <div class="l-content">
          <div class="l-gutter">
            <div class="l-table width-full">
              <div class="l-table-cell">
                <a class="header-logo" href="<?php bloginfo ('url'); ?>">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-1.jpg" />
                </a>
              </div>
              <div class="l-table-cell align-right align-middle">
                <div class="header-bannercontact display-pc">
                  <p class="header-bannercontact-msg">
                    お電話でのお問い合わせ・ご注文
                  </p>
                  <p class="header-bannercontact-num">
                    0120-019-522
                  </p>
                  <p class="header-bannercontact-time">
                    平日10：00ー18：00
                  </p>
                </div>

                <nav class="nav-header nav-header-slide display-sp">
                  <div id="js-nav-trigger" class="nav-trigger">
                    <div class="nav-trigger-bar nav-trigger-bar-top"></div>
                    <div class="nav-trigger-bar nav-trigger-bar-middle"></div>
                    <div class="nav-trigger-bar nav-trigger-bar-bottom"></div>
                  </div>
                </nav>

              </div>
            </div>
          </div>
        </div>

        <nav class="nav nav-header nav-header-pc display-pc">
          <div class="l-content">
            <div class="l-gutter">
              <ul class="l-grid l-grid-5cols">
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>">ホーム</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/reason">当店が選ばれる理由</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/itemlist">取り扱い商材一覧</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/hosyou">安心保証</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/staff">スタッフ紹介</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>    
        
        <nav class="nav nav-header nav-header-sp display-sp ">
          <div class="l-content">
            <div class="l-gutter">
              <ul class="l-grid l-grid-4cols">
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/reason">当店が選ばれる理由</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/itemlist">取り扱い商材一覧</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/hosyou">安心保証</a>
                </li>
                <li class="l-grid-col">
                  <a href="<?php bloginfo ('url'); ?>/staff">スタッフ紹介</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>    

      </div>



      <!-- wrapper start -->
      <div class="wrapper">

        <div class="l-content">
          <div class="l-gutter">



         <!-- header start -->
         <!-- <div id="header">
            <h1><a href="index.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header_logo.gif" alt="全国対応のチェーンソー（チェンソー）専門店" title="全国対応のチェーンソー（チェンソー）専門店" /></a></h1>
            <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header_tel.gif" /></p>
            <ul id="g_navi">
               <li><a href="index.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/new_menu/menu_01.gif" alt="チェンソー.com HOME" /></a></li>
               <li><a href="reason.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/new_menu/menu_02.gif" alt="当店が選ばれる理由" /></a></li>
               <li><a href="itemlist.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/new_menu/menu_03.gif" alt="取り扱い商材一覧" /></a></li>
               <li><a href="hosyou.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/new_menu/menu_04.gif" alt="安心保証" /></a></li>
               <li><a href="staff.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/new_menu/menu_05.gif" alt="スタッフ紹介" /></a></li>
            </ul>
         </div> -->
         <p class="clear">
            <br />
         <p><img class="w-sp-res" src="<?php echo get_stylesheet_directory_uri(); ?>/img/topimg_01.jpg" alt="チェンソー.com" /></p>
         <!-- header end -->

