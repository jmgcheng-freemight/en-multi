<?php
/**
 * Chainsaw functions and definitions
 *
 */

define("TEMPALTE_DIRECTORY_URI", get_template_directory_uri());
define("BLOG_URL", get_site_url()); 
 
 
function chainsaw_setup() {

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	/*add_theme_support( 'title-tag' );*/

}
add_action( 'after_setup_theme', 'chainsaw_setup' );


remove_filter( 'the_content', 'wpautop' );


function set_content_url($s_content)
{
	$s_content = str_replace('{{TEMPALTE_DIRECTORY_URI}}', TEMPALTE_DIRECTORY_URI, $s_content);
	$s_content = str_replace('{{BLOG_URL}}', BLOG_URL, $s_content);
	return $s_content;
}
add_filter('the_content','set_content_url'); 