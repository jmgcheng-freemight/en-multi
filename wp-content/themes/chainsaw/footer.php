

         <!-- footer start -->
         <div class="footer">
            <img class="w-sp-res" src="<?php echo get_stylesheet_directory_uri(); ?>/img/foot.jpg" alt="「チェーンソー（チェンソー）」をお探しなら全国対応のチェーンソー（チェンソー）専門店、５００日間保証のチェーンソー.com" />
         </div>
         <!-- footer end -->



          </div> <!-- .l-gutter -->
        </div> <!-- .l-content -->

         
      </div>
      <!-- wrapper end -->
      <!-- リマーケティング タグの Google コード -->
      <!--
         リマーケティング タグは、個人を特定できる情報と関連付けることも、デリケートなカテゴリに属するページに設置することも許可されません。タグの設定方法については、こちらのページをご覧ください。
         http://google.com/ads/remarketingsetup
         -->
      <script type="text/javascript">
         /* <![CDATA[ */
         var google_conversion_id = 896163192;
         var google_custom_params = window.google_tag_params;
         var google_remarketing_only = true;
         /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
      <noscript>
         <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/896163192/?value=0&amp;guid=ON&amp;script=0"/>
         </div>
      </noscript>

      <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/scripts.js"></script>

      <?php wp_footer(); ?>
      
      </div> <!-- #js-container-whole -->


      <aside class="aside-slide display-sp">
         
         <nav class="side-nav">
            <h3 class="side-nav-h">
               メインメニュー
            </h3>
            <ul>
               <li>
                  <a href="<?php bloginfo ('url'); ?>">トップページ</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/guide">納期・送料・お支払い</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/hosyou">安心保証</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/reason">当店が選ばれる理由</a>
               </li>
            </ul>
         </nav>

         <br>

         <nav class="side-nav">
            <h3 class="side-nav-h">
               商品カテゴリ
            </h3>
            <h4 class="side-nav-h-sub">
               用途別から選ぶ
            </h4>
            <ul>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/purpose_a">初めての方向け</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/purpose_b">日曜大工向け</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/purpose_c">剪定・枝打ち向け</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/purpose_d">薪作り向け</a>
               </li>
               <li>
                  <a href="<?php bloginfo ('url'); ?>/purpose_e">林業・造園業者向け</a>
               </li>
            </ul>
            <h4 class="side-nav-h-sub">
               排気量から選ぶ
            </h4>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/size_a">小型チェーンソー</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/size_b">標準チェーンソー</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/size_c">パワフルチェーンソー</a></li>
            </ul>
            <h4 class="side-nav-h-sub">
               切断長から選ぶ
            </h4>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/cut_a">～25cm</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/cut_b">～40cm</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/cut_c">40cm～</a></li>
            </ul>
            <h4 class="side-nav-h-sub">
               価格帯から選ぶ
            </h4>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/price_a">～￥40,000</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/price_b">～￥60,000</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/price_c">￥60,000～</a></li>
            </ul>
            <h4 class="side-nav-h-sub">
               一覧から選ぶ
            </h4>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/itemlist">カテゴリ別一覧</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/shiyoubetsu">仕様別一覧</a></li>
            </ul>
            <h4 class="side-nav-h-sub">
               関連商品を選ぶ
            </h4>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/shiyoubetsu_spare-chain">ソーチェン</a></li>
               <li><a href="#">ガイドバー</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/shiyoubetsu_mainte">目立て用品</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/shiyoubetsu_protection">防護用品</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/shiyoubetsu_oil">チェーンオイル</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/shiyoubetsu_oil#engine-oil">エンジンオイル</a></li>
               <li><a href="#">その他</a></li>
            </ul>
         </nav>

         <br>

         <nav class="side-nav">
            <h3 class="side-nav-h">
               店長のコメント
            </h3>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/profile">店長のコメント</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/point">チェンソーの選び方</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/recommend">お勧めチェンソーご紹介</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/kodawari">シングウへのこだわり</a></li>
            </ul>
         </nav>

         <br>

         <nav class="side-nav">
            <h3 class="side-nav-h">
               会社概要
            </h3>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/staff">スタッフ紹介</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/company">会社概要</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/useguide">ご利用ガイド</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/contact">お問合せ（メール）</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/tel">お問合せ（電話）</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/sitemap">サイトマップ</a></li>
            </ul>
         </nav>

         <br>

         <nav class="side-nav">
            <h3 class="side-nav-h">
               お役立ち情報
            </h3>
            <ul>
               <li><a href="<?php bloginfo ('url'); ?>/teirehou">チェーンソーの手入れ方法</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/tukaikata">チェンーソーの正しい使い方</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/medatehou">チェーンソーの目立て方法</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/teirehou">チェーンソーのメンテナンス方法</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/hokanhou">チェーンソーの保管</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/junbi">チェーンソーの始動前の準備</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/anzen">安全にチェンソーを使うために</a></li>
               <li><a href="<?php bloginfo ('url'); ?>/teirehou">チェン及びバーの正しい取り付け方</a></li>
               <li><a href="#">チェンソーの修理方法</a></li>
            </ul>
         </nav>         
         
      </aside>


      
   </body>
</html>