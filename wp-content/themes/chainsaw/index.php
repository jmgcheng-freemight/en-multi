<?php 
  get_header();
  
?>
        

         <!-- contents_area start -->
        <div class="container">
          <?php 
              get_sidebar(); 
            ?>
            <!-- main start -->
            
            
            <div class="main">
              <?php if ( have_posts() ) : ?>
                  <?php 
                    while( have_posts() ) :
                      the_post();
                  ?>

                        <?php
                          the_content();
                        ?>

                  <?php 
                    endwhile ;
                  ?>
              <?php endif ; ?>
   
            </div>
            <!-- main end -->
        </div>






<?php 

  get_footer(); 
?>