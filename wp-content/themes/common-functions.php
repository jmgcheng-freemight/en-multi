<?php

function send_handlift_mail($sendid,$email) {
	global $wpdb;
	
	$sender_email = get_custom('sender_email');
	$admin_receiver_email = get_custom('receiver_email');
	$bcc_email = get_custom('bcc_email');
	$email_message = nl2br(get_custom('email_message'));
	
	$myrows = $wpdb->get_results( "SELECT * FROM order_data WHERE sendid =".$sendid." AND email = '".$email."' ORDER BY id DESC LIMIT 1" );
	$orders = json_decode(json_encode($myrows), True);
	$order = $orders[0];
	
	$email_title = 'NE受注メール（'.$order['order_no'].')';

	/* $client_content= "<div>ご用命ありがとうございました。<br/>
		<br/>
		翌営業日内に、改めてお返事をさせて頂きます。<br/>
		今しばらく、お待ちくださいませ。<br/>
		<br/>
		何か、ご不明な点やご質問などございましたら気兼ねなくお問合せくださいませ。 <br/>
		どうぞ、よろしくお願いいたします。</div>"; */

	//send email
	$admin_email = get_option( 'admin_email' );
	$to = $order['email'];
	$subject = $email_title;
	
	/* $admin = 'yamaguchi@freemight.com';  */
	$payment_method = split(" ",$order['payment_method']); 
	if( $payment_method[0]=='代金引換(一括払い)'){
		$payment = '代金引換';
	} else if( $payment_method[0]=='銀行振込(一括払い)' ){
		$payment = '銀行振込前払い';
	} else {
		$payment = 'クレジットカード';
	}
	
	if( floatval($order['upsell_amount']) > 0 ) :
	$new_amount =  floatval($order['amount']) + floatval($order['upsell_amount']);
	$body ='<div style="color:black;">注文コード：'.$order['order_no'].'<br/>
			注文日時：'.$order['order_date'].'<br/>
			■注文者の情報<br/>
			氏名：'.$order['order_name'].'<br/>
			氏名（フリガナ）：'.$order['order_phonetic_name'].'<br/>
			郵便番号：'.$order['postal_code'].'<br/>
			住所：'.$order['address'].'<br/>
			電話番号：'.$order['tell_no'].'<br/>
			Ｅメールアドレス：'.$order['email'].'<br/>
			■支払方法<br/>
			支払方法：'.$payment.'<br/>
			■注文内容<br/>
			------------------------------------------------------------<br/>
			商品番号：'.$order['product_code'].'<br/>
			注文商品名： '.$order['product_name'].'<br/>
			商品オプション：<br/>
			単価：￥'.number_format($order['amount']).'<br/>
			数量：1<br/>
			小計：￥'.number_format($order['amount']).'<br/>
			------------------------------------------------------------<br/>
			商品番号：'.$order['upsell_number'].'<br/>
			注文商品名： '.$order['upsell_name'].'<br/>
			商品オプション：<br/>
			単価：￥'.number_format($order['upsell_amount']).'<br/>
			数量：1<br/>
			小計：￥'.number_format($order['upsell_amount']).'<br/>
			------------------------------------------------------------<br/>
			商品合計：￥'.number_format($new_amount).'<br/>
			税金：￥0<br/>
			送料：￥'.number_format($order['shipping_cost']).'<br/>
			手数料：￥0<br/>
			その他費用：￥0<br/>
			------------------------------------------------------------<br/>
			合計金額(税込)：￥'.number_format($order['total_amount']).'<br/>
			------------------------------------------------------------<br/>
			■届け先の情報<br/>
			[送付先1]<br/>
			　送付先1氏名：'.$order['order_name'].'<br/>
			　送付先1氏名（フリガナ）：'.$order['order_phonetic_name'].'<br/>
			　送付先1郵便番号：'.$order['postal_code'].'<br/>
			　送付先1住所：'.$order['address'].'<br/>
			　送付先1電話番号：'.$order['tell_no'].'<br/>
			　送付先1お届け方法：'.$order['delivery_method'].'<br/>
			■通信欄';
	else:
	$body ='<div style="color:black;">注文コード：'.$order['order_no'].'<br/>
			注文日時：'.$order['order_date'].'<br/>
			■注文者の情報<br/>
			氏名：'.$order['order_name'].'<br/>
			氏名（フリガナ）：'.$order['order_phonetic_name'].'<br/>
			郵便番号：'.$order['postal_code'].'<br/>
			住所：'.$order['address'].'<br/>
			電話番号：'.$order['tell_no'].'<br/>
			Ｅメールアドレス：'.$order['email'].'<br/>
			■支払方法<br/>
			支払方法：'.$payment.'<br/>
			■注文内容<br/>
			------------------------------------------------------------<br/>
			商品番号：'.$order['product_code'].'<br/>
			注文商品名： '.$order['product_name'].'<br/>
			商品オプション：<br/>
			単価：￥'.number_format($order['amount']).'<br/>
			数量：1<br/>
			小計：￥'.number_format($order['amount']).'<br/>
			------------------------------------------------------------<br/>
			商品合計：￥'.number_format($order['amount']).'<br/>
			税金：￥0<br/>
			送料：￥'.number_format($order['shipping_cost']).'<br/>
			手数料：￥0<br/>
			その他費用：￥0<br/>
			------------------------------------------------------------<br/>
			合計金額(税込)：￥'.number_format($order['total_amount']).'<br/>
			------------------------------------------------------------<br/>
			■届け先の情報<br/>
			[送付先1]<br/>
			　送付先1氏名：'.$order['order_name'].'<br/>
			　送付先1氏名（フリガナ）：'.$order['order_phonetic_name'].'<br/>
			　送付先1郵便番号：'.$order['postal_code'].'<br/>
			　送付先1住所：'.$order['address'].'<br/>
			　送付先1電話番号：'.$order['tell_no'].'<br/>
			　送付先1お届け方法：'.$order['delivery_method'].'<br/>
			■通信欄';
	endif;
	$headers = 'From:Hand-lift <'.$sender_email.'>' . "\r\n" ;
	$headers .='Reply-To: '.$sender_email. "\r\n" ;
	$headers .='Bcc: '.$bcc_email. "\r\n" ;
	$headers .='X-Mailer: PHP/' . phpversion();
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";   
	
	//email to admin
	mail($admin_receiver_email, $subject, $body,$headers); 
	
	$client_headers = 'From:Hand-lift <'.$sender_email.'>' . "\r\n" ;
	$client_headers .='Reply-To: '.$sender_email. "\r\n" ;
	$client_headers .='X-Mailer: PHP/' . phpversion();
	$client_headers .= "MIME-Version: 1.0\r\n";
	$client_headers .= "Content-type: text/html; charset=utf-8\r\n";
	//email to client
	mail( $to, $subject, $email_message, $client_headers);
	
	wp_die();
}

function get_news(){
	global $wpdb;
						
	$myrows = $wpdb->get_results( "SELECT * FROM wp_custom_options_plus WHERE name ='news'" );
	$news = json_decode(json_encode($myrows), True);
	$news = $news[0];
	
	return $news['value'];
}
?>